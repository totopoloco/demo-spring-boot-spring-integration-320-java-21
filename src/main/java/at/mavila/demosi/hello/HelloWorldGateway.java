package at.mavila.demosi.hello;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway(name = "helloWorldGateway")
@FunctionalInterface
public interface HelloWorldGateway {
  @Gateway(requestChannel = "helloWorldChannel")
  void sayHello(final String name);
}
