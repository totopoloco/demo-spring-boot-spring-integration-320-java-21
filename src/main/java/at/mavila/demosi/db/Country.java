package at.mavila.demosi.db;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.util.Date;
import lombok.Getter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity(name = "country")
@Getter
public class Country {

  @Id
  @GeneratedValue(
      strategy = jakarta.persistence.GenerationType.IDENTITY
  )
  private Integer id;

  @Column(
      name = "english_name",
      nullable = false,
      length = 100
  )
  private String englishName;

  @Basic
  @Column(name = "processed_time", insertable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date processedTime;

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
        .append("id", id)
        .append("englishName", englishName)
        .toString();
  }
}
