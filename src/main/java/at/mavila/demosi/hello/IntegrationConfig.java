package at.mavila.demosi.hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;

@Configuration

public class IntegrationConfig {

  @Bean
  public MessageChannel helloWorldChannel() {
    return new DirectChannel();
  }
}
