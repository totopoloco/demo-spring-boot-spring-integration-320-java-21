package at.mavila.demosi.db;

import jakarta.persistence.EntityManagerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.jpa.core.JpaExecutor;

@Configuration
public class JpaConfig {

  @Bean
  public JpaExecutor jpaExecutor(final EntityManagerFactory entityManagerFactory) {
    final JpaExecutor executor = new JpaExecutor(entityManagerFactory);
    executor.setJpaQuery(" from country c where c.processedTime is null");
    return executor;
  }
}

