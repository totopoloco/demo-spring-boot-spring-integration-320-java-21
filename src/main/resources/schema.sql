create table COUNTRY
(
    ID             int          not null AUTO_INCREMENT,
    ENGLISH_NAME   varchar(100) not null,
    PROCESSED_TIME timestamp    null,
    PRIMARY KEY (ID)
);