package at.mavila.demosi.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.jpa.core.JpaExecutor;
import org.springframework.integration.jpa.inbound.JpaPollingChannelAdapter;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableJpaRepositories
@Slf4j
public class CountryIntegrationConfig {


  private final JpaExecutor jpaExecutor;

  @Value("${country.poller.cron}")
  public String cronExpression;

  public CountryIntegrationConfig(JpaExecutor jpaExecutor) {
    this.jpaExecutor = jpaExecutor;
  }

  @Bean
  public MessageChannel countryChannel() {
    return new DirectChannel();
  }

  @Bean
  public MessageChannel updateCountryChannel() {
    return new DirectChannel();
  }

  @Bean
  @InboundChannelAdapter(value = "countryChannel", poller = @Poller(cron = "#{@countryIntegrationConfig.cronExpression}"))
  public MessageSource<Object> countryMessageSource(){
    return new JpaPollingChannelAdapter(this.jpaExecutor);
  }


}