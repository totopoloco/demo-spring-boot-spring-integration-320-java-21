package at.mavila.demosi.hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloWorldController {
  private final HelloWorldGateway helloWorldGateway;

  public HelloWorldController(HelloWorldGateway helloWorldGateway) {
    this.helloWorldGateway = helloWorldGateway;
  }

  @GetMapping("/hello/{name}")
  public void sayHello(@PathVariable final String name) {
    log.info("Hello, {}!", name);
    this.helloWorldGateway.sayHello(name);
  }
}