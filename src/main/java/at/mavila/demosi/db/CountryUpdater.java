package at.mavila.demosi.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class CountryUpdater {

  private final CountryRepository countryRepository;

  public CountryUpdater(CountryRepository countryRepository) {
    this.countryRepository = countryRepository;
  }

  @ServiceActivator(inputChannel = "updateCountryChannel")
  public void updateCountries(List<Integer> countries) {
    countries.forEach(countryRepository::updateProcessedTime);
  }
}