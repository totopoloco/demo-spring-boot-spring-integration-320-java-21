package at.mavila.demosi.db;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;

@MessagingGateway
public interface CountryGateway {

  @Payload("new java.util.Date()")
  void pollCountries();
}
