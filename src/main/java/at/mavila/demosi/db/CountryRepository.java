package at.mavila.demosi.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CountryRepository extends JpaRepository<Country, Integer> {

  @Modifying
  @Transactional
  @Query("update country c set c.processedTime = current_timestamp where c.id = :id")
  void updateProcessedTime(@Param("id") Integer id);
}