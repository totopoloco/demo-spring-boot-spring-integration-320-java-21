package at.mavila.demosi.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class CountryProcessor {

  @Transformer(inputChannel = "countryChannel", outputChannel = "updateCountryChannel")
  public List<Integer> processCountries(List<Country> countries) {

    List<String> collect = countries.stream().map(Country::toString).toList();
    log.info("Countries transformed: {}", collect);

    //Apply some filtering and return only valid id's
    return countries.stream().map(Country::getId).toList();

  }
}