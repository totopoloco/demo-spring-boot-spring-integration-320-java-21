package at.mavila.demosi.hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

@Component("helloWorldService")
@Slf4j
public class HelloWorldService {
  @ServiceActivator(inputChannel = "helloWorldChannel")
  public void sayHello(final String name) {
    log.info("Hello, {}!", name);
  }
}