package at.mavila.demosi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EntityScan(basePackages = "at.mavila.demosi.db")
@EnableIntegration
public class DemosiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemosiApplication.class, args);
	}

}
